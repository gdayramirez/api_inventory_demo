'use strict';
const database = require('../database'),
	mongoose = require('mongoose'),
	autoIncrement = require('mongoose-auto-increment'),
	Schema = mongoose.Schema;

let users = new Schema({
	_id: Number,
	name: String,
	email: String,
    password: String,
    rol:String
});
users.plugin(autoIncrement.plugin, 'users');
module.exports = mongoose.model('users', users, 'users');