'use strict'

const api = require('express').Router()

const oauth = require('../controllers/oauth')
const validator = require('../controllers/validator')
const headers = require('../config/headers')

module.exports = (()=>{
    api.post('/',
        validator.userExists,
        oauth.getWebToken
    )
    api.get('/valid/:token',
        oauth.validWebToken
    )
    return api;
})()