'use strict';
const database = require('../database'),
	mongoose = require('mongoose'),
	autoIncrement = require('mongoose-auto-increment'),
	Schema = mongoose.Schema;

let items = new Schema({
	_id: {
		type: Number,
		unique: true
	},
	name: String,
	description: String,
    price: String,
    image:String
});
items.plugin(autoIncrement.plugin, 'items');
module.exports = mongoose.model('items', items, 'items');