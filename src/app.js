'use strict';
const express = require('express');
const app = express();
app.use(require('body-parser').urlencoded({extended: false}));
app.use(require('body-parser').json());
app.use('/', require('./routes/api'));
app.use('/create', require('./routes/post'));
app.use('/update', require('./routes/put'));
app.use('/get', require('./routes/get'));
app.use('/upload/', require('./routes/upload'));
app.use('/delete/', require('./routes/delete'));
app.use('/oauth/', require('./routes/oauth'));
app.listen(3000, () => {
    console.log('ADS API http://127.0.0.1:' + 3000);
});