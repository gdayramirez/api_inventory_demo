'use strict'

const items = require('../models/items')

exports.deleteData = (req,res,next)=>{
    console.log('delete data')
    items.remove({
        _id:req.params.id
    },(err,resp)=>{
        if(err) return res.status(400).send({
            message : 'Ha ocurrido un error interno'
        })

        res.send({
            message : 'El producto ha sido eliminado'
        })
    })
}