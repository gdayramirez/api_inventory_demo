'use strict'
const items = require('../models/items')

exports.saveData = (req,res,next)=>{
    console.log('create.saveData')
    new items(req.body).save((err,resp)=>{
        if(err) return res.status(500).send({
            message : 'Ha ocurrido un error interno'
        })
        res.send({
            message : 'Se ha guardado con exito la informacion',
            id : resp._id
        })
    })
}