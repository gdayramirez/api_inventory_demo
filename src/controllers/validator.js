'use strict'

const items = require('../models/items')
const users = require('../models/users')

const values = {
    POST:['name','description','price'],
    PUT :[]
}


exports.init = (req,res,next)=>{
    console.log('post.init')
    let errors = []
    console.log(req.method)
    values[req.method].forEach((e)=>{
        if(!req.body[e]){
            errors.push({
                error : 'field '+e+' is required in request'
            })
        }
    })

    if(errors.length > 0) return res.status(401).send({
        message : 'Se han presentado algunos errores',
        error : errors
    })
    next()
}

exports.exists = (req,res,next)=>{
    console.log('validator exists')
    items.findOne({
        _id:req.params.id
    },(err,resp)=>{
        if(err) return res.status(401).send({
            message : 'Ha ocurrido un error interno'
        })
        if(!resp) return res.status(404).send({
            message : 'El producto '+req.params.id +' no se ha encontrado en el registro'
        })
        next()
    })
}

exports.userExists =  (req,res,next)=>{
    console.log('userExists')
    users.findOne({
        email : req.body.email,
        password : req.body.password
    },(err,resp)=>{
        if(err) return res.status(500).send({
            message : 'Ha ocurrido un error interno'
        })
        if(!resp) return res.send({
            message : 'El usuario solicitado no se encuentra disponible'
        })
        next()
    })
}