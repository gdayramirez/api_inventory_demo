'use strict'
const path = require('path');
require('dotenv').config({ path: path.join(__dirname, '/.env') });

const AWS = require('aws-sdk')
const fs = require('fs')
const multiparty = require('multiparty');


exports.prepareS3 = (req, res, next) => {
    console.log(process.env.ACCESS_KEY)
    console.log('image upload')
    AWS.config.update({
        accessKeyId: process.env.ACCESS_KEY,
        secretAccessKey: process.env.SECRET_ACCESS_KEY,
        region: 'sa-east-1'
    })
    req.S3 = new AWS.S3({
        params: {
            Bucket: process.env.BUCKET
        }
    });
    next()
}


exports.uploadImage = (req, res, next) => {
    console.log('upload image')
    let form = new multiparty.Form();
    form.parse(req, function (err, fields, files) {
        if (err) return res.send({
            message: 'Ha ocurrido un error',
            data: 'error : ' + err
        })
        fs.readFile(files.file[0].path, function (err, data) {
            if (err) return res.send({
                message: 'Ha ocurrido un error con la carga de la imagen',
                error: 'Error :' + err
            })
            console.log(files.file[0].originalFilename)
            req.S3.upload({
                Key: files.file[0].originalFilename,
                Body: data
            }, function (err, data) {
                if (err) return res.status(500).send({
                    message: 'Ha ocurrido un error al subir la imagen',
                    error: 'Error : ' + err
                })
                console.log('Imagen subida con exito')
                res.send({
                    message: 'La imagen se ha subido con exito',
                    data: data
                })
            });
        });
    });
}
