'use strict'

const api = require('express').Router()
const get = require('../controllers/search')
const headers = require('../config/headers')
module.exports = (()=>{
    api.get('/',
        get.list
    )
    api.get('/:id',
        get.onlyOne
    )
    return api;
})()