'use strict'
const secret = 'Esta_es_mi_llave_para_mi_oauth_service'
const jwt = require('jwt-simple');
const moment = require('moment')


exports.getWebToken = (req, res, next) => {
    console.log('get web token')

    try {
        let payload = {
            email: req.body.email,
            password: req.body.password,
            iat: moment().unix(),
            exp: moment().add(5, 'hours').unix()
        }
        let token = jwt.encode(payload, secret);
        res.send({
            message: 'success',
            code: 200,
            token: token,
        })
    } catch (e) {
        res.staus(403).send({
            message: 'Ha ocurrido un error interno',
            error: 'Error ' + err
        })
    }

}

exports.validWebToken = (req, res, next) => {
    console.log('get web token')

    try {
        let token = jwt.decode(req.params.token, secret);
        res.send(token)
    } catch (e) {
        res.status(403).send({
            message: 'web token invalido',
            error: 'Error ' + e
        })
    }
    
}