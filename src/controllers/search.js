'use strict'

const items = require('../models/items')


exports.list = (req,res,next)=>{
    console.log('Este es mi listado')
    items.find({

    },(err,resp)=>{
        if(err || resp.length < 1) return res.status(500).send({
            message : 'Ha ocurrido un error interno'
        })
        let results = []
        resp.forEach((e)=>{
            results.push({
                id : e._id,
                name : e.name,
                description : e.description,
                price : e.price,
                image : e.image ? e.image : ''
            })
        })
        res.send({
            status : 'success',
            code : 200,
            data : results
        })
    })   
}

exports.onlyOne = (req,res,next)=>{
    items.findOne({
        _id: req.params.id
    },(err,resp)=>{
        if(err) return res.status(500).send({
            message : 'Ha ocurrido un error interno',
            error : 'Error '+err
        })
        if(!resp) return res.status(404).send({
            message : 'No se localizado el producto'
        }) 
        res.send({
            message : 'success',
            code : 200,
            data : resp
        }) 
    })
}