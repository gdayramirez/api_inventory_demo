'use strict'

const api = require('express').Router()

const validator = require('../controllers/validator')
const update = require('../controllers/update')

module.exports = (()=>{
    api.put('/:id',
        validator.init,
        validator.exists,
        update.updateData
    )
    return api;
})()