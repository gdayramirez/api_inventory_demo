'use strict'

const api = require('express').Router()
const validator = require('../controllers/validator')
const _delete = require('../controllers/delete')

module.exports = (()=>{
    api.delete('/:id',
        validator.exists,
        _delete.deleteData
    )
    return api;
})()