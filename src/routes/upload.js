'use strict'

const api = require('express').Router()

const upload = require('../controllers/upload')

module.exports = (()=>{
    api.post('/image/',
       upload.prepareS3,
       upload.uploadImage
    )
    return api;
})()