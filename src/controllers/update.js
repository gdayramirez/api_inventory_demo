'use strict'

const items = require('../models/items')

exports.updateData = (req,res,next)=>{
    console.log('update.updateData')
    items.update({
        _id : req.params.id
    },{
        $set : req.body
    },(err,resp)=>{
        if(err) return res.status(500).send({
            message : 'Ha ocurrido un error interno'
        })
        res.send({
            message : 'El producto '+req.params.id +' ha sido actualizado'
        })
    })
}